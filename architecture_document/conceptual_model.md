# Gaia-X Conceptual Model

The Gaia-X conceptual model, shown in the figure below, describes all concepts
in the scope of Gaia-X and their relation to each other. Supplementary,
more detailed models may be created in the future to specify further
aspects. The general interaction pattern is further depicted in section
[Basic Interactions of Participants].

The Gaia-X core concepts are represented in classes. An entity
highlighted in blue shows that an element is part of Gaia-X and
therefore described by a Gaia-X Self-Description. The upper part of the
model shows different actors of Gaia-X, while the lower part shows
elements of commercial trade and the relationship to actors outside
Gaia-X.

![](media/image2.png)
*Gaia-X conceptual model*

## Participants

A Participant is an entity, as defined in ISO/IEC 24760-1 as "item
relevant for the purpose of operation of a [domain](federation_service.md#identity-and-trust) that has
recognizably distinct existence"[^7], which is onboarded and has a
Gaia-X Self-Description. A Participant can take on one or multiple of
the following roles: Provider, Consumer, Federator. Section [Federation Services]
demonstrates use cases that illustrate how these roles could be filled.
Provider and Consumer present the core roles that are in a
business-to-business relationship while the Federator enables their
interaction.

[^7]: ISO/IEC. IT Security and Privacy — A framework for identity management: Part 1: Terminology and concepts (24760-1:2019(en)). ISO/IEC. <https://www.iso.org/obp/ui/#iso:std:iso-iec:24760:-1:ed-2:v1:en>


### Provider

A Provider is a Participant who provides Assets and Resources in the
Gaia-X Ecosystem. It defines the Service Offering including terms and
conditions as well as technical Policies. Further, it provides the
Service Instance that includes a Self-Description and
Policies. Therefore, the Provider operates different Resources and
possesses different Assets.

### Federator

Federators are in charge of the Federation Services and the Federation
which are autonomous of each other. Federators are Gaia-X Participants.
There can be one or more Federators per type of Federation Service.

A Federation refers to a loose set of interacting actors that directly
or indirectly consume, produce, or provide Assets and related Resources.

### Consumer

A Consumer is a Participant who searches Service Offerings and consumes
Service Instances in the Gaia-X Ecosystem to enable digital offerings
for End-Users.

## Resources and Assets

Resources and Assets describe in general the goods and objects of a
Gaia-X Ecosystem and are defined as follows. Resources and Assets
compose the Service Offerings.

### Assets

An Asset is an element which does not expose an Endpoint and is used to
compose the Service Offering. An Endpoint is defined according to ISO
ISO/TR 24097-3:2019(en) as a combination of a binding a network
address[^8]. An Asset can be a Data Asset, a Software Asset, a Node or
an Interconnection Asset. A set of Policies is tied to each Asset. The
different categories of Assets are visualized in Figure 3 and defined
below:

[^8]: ISO/IEC. Intelligent transport systems – Using web services (machine-machine delivery) for ITS service delivery (ISO/TR 24097-3:2019(en)).
<https://www.iso.org/obp/ui/fr/#iso:std:iso:tr:24097:-3:ed-1:v1:en>

<figure>
```mermaid
 classDiagram
      class Asset{
          class ()
          policies ()
      }
      class Data Asset{
      }
      class Software Asset{
      }
      class Node{
      }
      class Interconnection{
      }
      Asset <|-- Data Asset
      Asset <|-- Software Asset
      Asset <|-- Node
      Asset <|-- Interconnection
```
<figcaption>Asset Categories</figcaption>
</figure>

A Data Asset is an Asset that consist of data in any form and necessary
information for data sharing.

A Node is an Asset and represents a computational or physical entity
that hosts, manipulates, or interacts with other computational or
physical entities.

A Software Asset is a form of Assets that consist of non-physical
functions.

An Interconnection as an Asset presents the connection between two
or multiple Nodes. These Nodes are usually deployed in different infrastructure 
domains and owned by different stakeholders, such as Consumers and/or Providers.
The Interconnection between the Nodes can be seen as a path which exhibits special 
characteristics, such as latency, bandwidth and security guarantees, that go beyond 
the characteristics of a path over the public Internet.

### Resources

Resources expose an Endpoint and compose a Service Offering. They are
bound to certain Policies.

The difference between Resources and Assets can be described as follows:
Resources represent those elements necessary to supply Assets. They can
be explained as internal Service Instances not available for order. For
example, the running instance that provides a data set is a Resource.

### Policies

Policy is defined as a statement of objectives, rules, practices, or regulations
governing the activities of Participants within Gaia-X.
From a technical perspective Policies are statements, rules or
assertions that specify the correct or expected behaviour of an
entity[^9][^10].

The [Policy Rules Document](https://gaia-x.eu/pdf/Gaia-X_Policy%20Rules_Document_2104.pdf) explains the general Policies defined by the Gaia-X association for all
Providers and Service Offerings. They cover, for example, privacy or cyber security policies
and are expressed in the conceptual model indirectly via the Gaia-X Federation Service Compliance
and as attributes of the Assets, Resources, Service Offerings, and Service Instances.


These general Policies form the basis for detailed Policies for a particular Service Offering, 
which can be defined additionally and contain particular restrictions and obligations defined by
the respective Provider or Consumer. They occur either as a Provider Policy (alias Usage Policies)
or a Consumer Policy (alias Search Policy):

- A Provider Policy/Usage Policy constraints the Consumer's use of an
Asset or Resource. _For example, a Usage Policy for data can constrain the use of the data by allowing to use it only for x times or for y days._

- A Consumer Policy describes a Consumer's restrictions of a 
requested Asset or Resource. _For example, a Consumer gives the restriction that a Provider of a certain service has to fulfil demands such as being located in a particular jurisdiction or fulfil a certain service level._

In the conceptual model, they appear as attributes in all elements related to Assets and Resources.
The specific Policies have to be in line with the general Policies in the [Policy Rules Document](https://gaia-x.eu/pdf/Gaia-X_Policy%20Rules_Document_2104.pdf).


[^9]: Singhal, A., Winograd, T., & Scarfone, K. A. (2007). Guide to secure web services: Guide to Secure Web Services - Recommendations of the National Institute of Standards and Technology. Gaithersburg, MD. NIST. <https://csrc.nist.gov/publications/detail/sp/800-95/final https://doi.org/10.6028/NIST.SP.800-95>              
[^10]: Oldehoeft, A. E. (1992). Foundations of a security policy for use of the National Research and Educational Network. Gaithersburg, MD. NIST. <https://doi.org/10.6028/NIST.IR.4734>                  


```mermaid
graph TD;
    A[Policy Rules] --> |defines| B[general Gaia-X Policies]
    B --> |basis for| C[Asset/Resource-specific Policies]
    C -->D[Provider Policy/ Usage Policy]
    C -->E[Consumer Policy/ Search Policy]
```

## Federation Services

Federation Services are services required for the operational
implementation of a Gaia-X Data Ecosystem. They are explained in greater
detail in the [Federation Service](federation_service.md) section.

They comprise four groups of services that are necessary to enable
Federation of Assets, Resources, Participants and interactions between
Ecosystems. The four service groups are Identity and Trust, Federated
Catalogue, Sovereign Data Exchange and Compliance.

## Service Offering

A Service Offering is defined as a set of Assets and Resources which a Provider aggregates and publishes as a single entry in a Catalogue. Service Offerings may themselves be aggregated realizing service composition. The instantiation of a Service Offering is the deliverable of a Provider to a Consumer. The Federation Services provide the foundation
for Service Offerings and the Service Offering uses and conforms to the
Federation Services.

## Additional Concepts 

In addition to those concepts and their relations mentioned above,
further ones exist in the conceptual model that are not directly
governed by Gaia-X. These concepts do not need to undergo any procedures
directly related to Gaia-X, e.g. do not create or maintain a Gaia-X
Self-Description.

First, the Service Instance realizes a Service Offering and can be used
by End-Users while relying on a contractual basis.

Second, Contracts are not in scope of Gaia-X but present the legal basis
for the Services Instances and include specified Policies. Contract
means the binding legal agreement describing a Service Instance and
includes all rights and obligations. This comes in addition to the
automated digital rights management embedded in every entity's
Self-Description.

Further relevant actors exist outside of the Gaia-X scope in terms of
End-Users and Asset Owners.

Asset Owners, e.g. data owners, describe a natural or legal person,
which holds the rights of an Asset that will be provided according to
Gaia-X regulations by a Provider and legally enable its provision.
As Assets are bundled into a Service Offering and nested Asset compositions can be possible,
there is no separate resource owner as well. Assets and resources can only realize together 
in a Service Offering and Service Instance by a Provider, which presents no need to model a 
separate legal holder of ownership rights.


End-Users use digital offerings of a Gaia-X Consumer that are enabled by
Gaia-X. The End-User uses the Service Instances containing
Self-Description and Policies.
