## Provider

A [Participant](#participant) who provides [Assets](#asset) and [Resources](#resource) in the GAIA-X ecosystem.

Note: The service(s) offered by a Provider are cloud and/or Edge services. Thus, the Provider will typically be acting as a Cloud Service Provider (CSP) to their [Consumers](#consumer).
