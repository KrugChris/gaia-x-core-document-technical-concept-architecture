## Node

A Node represents a computational or physical entity that hosts, manipulates, or interacts with other computational or physical entities.

A node can contain other nodes as sub-nodes so that a hierarchy of nodes is established.

### references
- Archimate 3.1 (2019)
