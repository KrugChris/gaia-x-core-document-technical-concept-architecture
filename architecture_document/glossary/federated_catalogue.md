## Federated Catalogue

Federated Catalogue is a [Gaia-X Federation Service](#federation-services).
It enables the discovery and selection of [Providers](#provider) and [Service Offerings](#service-offering) in a Gaia-X Ecosystem.
