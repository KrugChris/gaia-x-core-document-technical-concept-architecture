## Usage Control

Usage Control is a technical mechanism to enforce usage restrictions in form of [Usage Policies](#usage-policy) after access has been granted and is concerned with requirements that pertain to future usages (obligations), rather than (e.g. data) access (provisions).
