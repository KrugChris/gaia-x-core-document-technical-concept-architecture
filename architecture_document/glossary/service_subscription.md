## Service Subscription

A Service Subscription is an agreement (contract) between a [Consumer](#consumer) and a [Provider](#provider), to allow and regulate the usage of one or more [Service Instances](#service-instance). It is related to a specific version of a [Service Offering](#service-offering) from which it derives the attributes of the [Service Instances](#service-instance) to be provisioned. The Service Subscription has a distinct lifecycle from the [Service Offering](#service-offering) and additional attributes and logic.
