## Digital Rights Management

Digital Rights Management (DRM) is the use of technical means to ensure that the authorised recipient of licensed content is limited to the rights that have been granted under license.

While the term DRM is usually associated with the protection of high-value media such as movies and television delivered to consumers, the subtype [Information Rights Management](#information-rights-management) is sometimes used to ensure correct usage of enterprise data.

DRM of all kinds usually involves the delivery of content in an encrypted form that requires both authorised/certified client software and a valid license to access.

The receiver is then able to access the content through the unlocked client which can enforce any required restrictions.
