## Accreditation

Accreditation is the third-party attestation related to a [Conformity Assessment Body](#conformity-assessment-body) conveying formal demonstration of its competence to carry out specific [Conformity Assessment](#conformity-assessment) tasks.

### references

- ISO/IEC 17000:2004(en)
