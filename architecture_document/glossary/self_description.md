## Self-Description

A Self-Description expresses characteristics of an [Asset](#asset), [Resource](#resource), [Service Offering] or [Participant](#participant) and describes properties and [Claims](#claim) while being tied to the Identifier.

### alias
- GAIA-X Self-Description
