## Data Sovereignty Service

Data Sovereignty Service is a [Gaia-X Federation Service](Federation Services.md). 
It enables the sovereign exchange and use of data in a Gaia-X Ecosystem using digital [Policies](#policy) to enforce control of data flow and providing transparency of data usages.
