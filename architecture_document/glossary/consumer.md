## Consumer

A Consumer is a [Participant](#participant) who consumes [Service Instance](#service-instance) in the GAIA-X ecosystem to enable digital offerings for [End Users](#end-user)

Note: A GAIA-X Consumer will act as a Cloud Service Customer (CSC) to the relevant [Provider](#provider), but will probably also be offering cloud and/or Edge services and thus acting as a Cloud Service Provider (CSP) in their own right to the customers and partners of their own business. These are considered [End Users](#end-user) from a Gaia-X perspective. 
